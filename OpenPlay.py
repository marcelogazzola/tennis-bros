from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

import time

browser = webdriver.Chrome()
browser.maximize_window()
browser.get('https://www.openplay.co.uk/login#login')

# Click on existing user
existingUser = browser.find_element_by_link_text('Existing User')
existingUser.click()

time.sleep(2)

email = browser.find_element_by_id('loginEmail')
email.send_keys('marcelogr' + Keys.TAB)

password = browser.find_element_by_id('loginPassword')
password.send_keys('DUqj3T3zkh7yiSs' + Keys.RETURN)

# Click on PLAYER FINDER
players = browser.find_element_by_xpath('//*[@id="dropdown-finder"]/a')
players.click()

time.sleep(3)

# Filter Tennis
tennis = browser.find_element_by_xpath('//*[@id="finderTop-facets"]/ul[1]/li[1]/a')
tennis.click()

time.sleep(3)

# Filter the location
# location = browser.find_element_by_css_selector('span.select2-selection__placeholder')
# location.click()

loc = browser.find_element_by_class_name('select2-search__field')
loc.send_keys('Dartford' + Keys.RETURN)

i = 1

for cont in range(3):
    time.sleep(3)
    messageMe = browser.find_element_by_xpath('//*[@id="finderTop-hits"]/div[{}]/div/div/h3/sup'.format(i))
    messageMe.click()
    frame = browser.find_element_by_xpath('//*[@id="smallModal-frame"]')
    url = frame.get_attribute('src')
    browser.get(url)
    msg = browser.find_element_by_id('content')
    msg.clear()
    msg.send_keys('Hi, I work in Dartford and I am looking for somebody to play with, any weekday, at evenings. '
                  'I am member on David Lloyd club, u too? If yes, would be perfect '
                  'to us to play there.' + Keys.TAB + Keys.RETURN)

    time.sleep(2)

    browser.get('https://www.openplay.co.uk/')

    # Click on PLAYER FINDER
    players = browser.find_element_by_xpath('//*[@id="dropdown-finder"]/a')
    players.click()

    time.sleep(3)

    # Filter Tennis
    tennis = browser.find_element_by_xpath('//*[@id="finderTop-facets"]/ul[1]/li[1]/a')
    tennis.click()

    i += 1
